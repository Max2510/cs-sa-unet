# CS-SA Unet

This repository provides the official implementation of the CS-SA Unet model with a consistency regularization.

## Requirements
- albumentations==1.0.3
- einops==0.3.2
- joblib==1.0.1
- numpy==1.21.1
- opencv_python==4.5.3.56
- Pillow==9.5.0
- pypng==0.20220715.0
- pytorch_forecasting==0.10.3
- pytorch_lightning==1.6.4
- pytorch_toolbelt==0.4.4
- PyYAML==6.0
- scikit_image==0.18.2
- scikit_learn==1.2.2
- scipy==1.10.1
- segmentation_models_pytorch==0.3.2
- SimpleITK==2.2.1
- skimage==0.0
- torch==1.9.0

## Getting Started
To run the training pipeline, the [SegTHOR dataset](https://competitions.codalab.org/competitions/21145#participate-get_starting_kit) should be downloaded and preprocessed.

To preprocess dataset and save preprocessed data, run `segthor_2d/preprocessing.py` script, previously changing paths to the raw data.

`python segthor_2d/preprocessing.py`

The following step is changing paths in the configuration file `segthor_2d/config/segthor_config.yaml`. The number of GPUS and training accelerator could be also changed in the configuration file.

## Usage

**Training**

`python segthor_2d/train.py`

**Ensembling predictions**

`python segthor_2d/ensemble.py`

**Experiments**

- To run single-model baseline, set `use_cps: False` in the configuration file.
- To run double-model baseline, set `use_cps: True, model/attn_params/use_attention: False, cps_weight: 0` in the configuration file.
- To run CS-SA Unet model, set `use_cps: True, model/attn_params/use_attention: True, cps_weight: 0` in the configuration file.
- To run CS-SA Unet model with consistency regularization, set `use_cps: True, model/attn_params/use_attention: True, cps_weight: 5` in the configuration file.

