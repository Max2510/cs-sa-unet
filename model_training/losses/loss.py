from torch import Tensor
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.loss import _Loss

from pytorch_toolbelt.losses import FocalLoss, BinaryLovaszLoss, DiceLoss


def get_loss(loss_config):
    """ Creates loss from config
    Args:
        loss_config (dict): dictionary of loss configuration:
        - name (str): loss name
        and other configs for specified loss
    """
    params = loss_config.get('params', dict())
    general_losses = {
        'l2_loss': nn.MSELoss,
        'multilabel': nn.MultiLabelSoftMarginLoss,
        'cross_entropy': nn.CrossEntropyLoss,
        'bce': nn.BCEWithLogitsLoss,
        'dice': DiceLoss,
        'bce_dice': BCEDice,
        'focal': FocalLoss,
        'focal_dice': FocalDice,
        'lovasz': BinaryLovaszLoss,
        'softmax_mse': Softmax_MSE,
        'kl_div': KL_div
    }

    return general_losses[loss_config['name']](**params)


class Softmax_MSE(_Loss):
    def __init__(self):
        super(Softmax_MSE, self).__init__()
        self.mse = nn.MSELoss()
        
    def forward(self, y_pred, y_true) -> Tensor:
        assert y_pred.size() == y_true.size()
        y_pred_softmax = F.softmax(y_pred, dim=1)
        y_true_softmax = F.softmax(y_true, dim=1)
        return self.mse(y_pred_softmax, y_true_softmax)
    
    
class KL_div(_Loss):
    def __init__(self):
        super(KL_div, self).__init__()
        
    def forward(self, y_pred, y_true) -> Tensor:
        assert y_pred.size() == y_true.size()
        y_pred_log_softmax = F.log_softmax(y_pred, dim=1)
        y_true_softmax = F.softmax(y_true, dim=1)
        return F.kl_div(y_pred_log_softmax, y_true_softmax)


class BCEDice(_Loss):
    def __init__(self, dice_params={}, nll_params={}, alpha=0.8, binary=False):
        super(BCEDice, self).__init__()
        if binary:
            self.nll = nn.BCEWithLogitsLoss(**nll_params)
        else:
            self.nll = nn.CrossEntropyLoss(**nll_params)
        self.dice = DiceLoss(**dice_params)
        self.alpha = alpha
        
    def forward(self, y_pred, y_true) -> Tensor:
        return (1 - self.alpha) * self.nll(y_pred, y_true) + self.alpha * self.dice(y_pred, y_true)
    
    
class FocalDice(_Loss):
    def __init__(self, dice_params={}, focal_params={}, alpha=0.8):
        super(FocalDice, self).__init__()
        self.focal = FocalLoss(**focal_params)
        self.dice = DiceLoss(**dice_params)
        self.alpha = alpha
        
    def forward(self, y_pred: Tensor, y_true: Tensor) -> Tensor:
        return (1 - self.alpha) * self.focal(y_pred, y_true) + self.alpha * self.dice(y_pred, y_true)