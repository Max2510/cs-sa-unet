import torch

def dice_metric(output, target, num_classes=5):
    target = torch.stack([(target==ch) for ch in range(1, num_classes)])
    output = torch.stack([(output==ch) for ch in range(1, num_classes)])
           
    intersection = torch.sum(output * target, dim=(1, 2, 3))
    union = torch.sum(target, dim=(1, 2, 3)) + torch.sum(output, dim=(1, 2, 3))
    union[union==0] = 1.
        
    dice_coeffs = 2 * intersection / union
    
    metrics = {'eso_dice': dice_coeffs[0].item(), 'heart_dice': dice_coeffs[1].item(), 
               'trac_dice': dice_coeffs[2].item(), 'aorta_dice': dice_coeffs[3].item(), 
               'dice': torch.mean(dice_coeffs).item()}
    
    return metrics

