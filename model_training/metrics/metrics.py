import torch
import torch.nn.functional as F
from collections import defaultdict
import numpy as np

from model_training.utils.common import cc_filter

class MetricCounter:
    """ Class which is used to store metrics
    """

    def __init__(self):
        self.metrics = defaultdict(list)

        
    def get_metrics(self):
        """ Calculates metric from cached list of metrics
        Returns:
            dict: dict of mean metric values
        """
        return {k: np.mean(v) for k, v in self.metrics.items()}

    
    def clear(self):
        """ Clears metrics cache
        """
        self.metrics = defaultdict(list)

        
    def add_metrics(self, metric_dict):
        """ Adds metrics to local cache
        Args:
            metric_dict (dict): dictionary with metrics
        """
        for metric_name in metric_dict:
            self.metrics[metric_name].append(metric_dict[metric_name])
            
            
    def calc_metrics(self, output, target, batch_nb=None, rank=None):
        """ Calc metrics and add it to local cache
        Args:
            output (torch.Tensor): Model output tensor
            target (torch.Tensor): Input labels
            batch_nb (int): Batch number.
        """
        pass
    
            
class MetricCalculator2D(MetricCounter):
    def __init__(self, calc_metrics_fn, slices_per_scans, ori_img_size=None, cc_filt=True, save_predictions_fn=None):
        super().__init__()
        self.slices_per_scans = slices_per_scans
        self.ori_img_size = ori_img_size
        self.cc_filt = cc_filt
        self.calc_metrics_fn = calc_metrics_fn
        self.save_predictions_fn = save_predictions_fn
        
        
    def calc_metrics(self, output, target, batch_nb, rank, save_predictions=False):
        if batch_nb==0:
            self.outputs = []
            self.targets = []
            self.scan_cntr = 0
            
        self.outputs.extend(output)
        if not target is None:
            if len(target.shape)==4 and target.shape[1]>1:
                target = target[:, -1]
            self.targets.extend(target)
        
        if len(self.outputs)>=self.slices_per_scans[rank][self.scan_cntr]:
            if not target is None:
                label = torch.stack(self.targets[:self.slices_per_scans[rank][self.scan_cntr]])
            
            logits = torch.stack(self.outputs[:self.slices_per_scans[rank][self.scan_cntr]])
            
            num_classes = logits.shape[1] - 1
            
            if logits.shape[1]>1:
                pred = logits.argmax(dim=1).type_as(logits)
            else:
                pred = (logits[:, 0]>0.5).type_as(logits)
            
            if self.cc_filt:
                pred = cc_filter(pred, num_classes)
            
            if not self.ori_img_size is None:
                y1, x1 = (self.ori_img_size[0] - pred.shape[1]) // 2, (self.ori_img_size[1] - pred.shape[2]) // 2
                y2, x2 = self.ori_img_size[0] - pred.shape[1] - y1, self.ori_img_size[1] - pred.shape[2] - x1
                
                pred = F.pad(pred, (x1, x2, y1, y2))
                
                if not target is None:
                    label = F.pad(label, (x1, x2, y1, y2))
                    
            if not target is None:
                metrics = self.calc_metrics_fn(pred, label)
                self.add_metrics(metrics)
                self.targets = self.targets[self.slices_per_scans[rank][self.scan_cntr]:]
            
            if not self.save_predictions_fn is None and save_predictions:
                self.save_predictions_fn(pred.cpu().numpy(), logits.cpu().numpy(), self.scan_cntr, rank)
            
            self.outputs = self.outputs[self.slices_per_scans[rank][self.scan_cntr]:]
            
            self.scan_cntr += 1 
            