import os
import torch
import torch.nn as nn
import math
import numpy as np

import cv2
from einops import rearrange
import time


class SelfAttention(nn.Module):
    def __init__(self, in_dim, pool_size=1, head_size=-1, factor=1, use_cos_sim=True, 
                 gamma=1., gamma_learnable=False, sigma=None, sigma_learnable=False, save_attentions=False, 
                 height=160, width=160, **params):
        super().__init__()
        if head_size<0:
            self.heads = 1
            self.in_dim_head = in_dim
        else:
            self.heads = int(in_dim / head_size)
            self.in_dim_head = head_size
        
        self.alpha = math.sqrt(self.in_dim_head)
        
        self.pool_size = pool_size
        if self.pool_size>1:
            self.pool = nn.MaxPool2d(self.pool_size, stride=self.pool_size)
        
        self.to_qkv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim//factor*3, kernel_size=1)
        self.conv_out = nn.Conv2d(in_channels=in_dim//factor, out_channels=in_dim, kernel_size=1)
        
        self.use_cos_sim = use_cos_sim
        
        if sigma is None:
            h = height // (in_dim // 32)
            w = width // (in_dim // 32)
            L = h * w // (self.pool_size ** 2)
            self.sigma = nn.Parameter(torch.ones(self.heads) * math.log2(L * (L - 1)))
        else:
            self.sigma = nn.Parameter(torch.ones([self.heads]) * sigma)
            
        if (not sigma is None) and (not sigma_learnable):
            self.sigma.requires_grad = False
        
        self.gamma = nn.Parameter(torch.Tensor([gamma]))
        if not gamma_learnable:
            self.gamma.requires_grad = False
        
        self.save_attentions = save_attentions
        

    def forward(self, x):
        b, c, h, w = x.shape
        
        q, k, v = tuple(rearrange(self.to_qkv(x), 'b (k c) y x -> k b c y x', k=3))
        
        if self.pool_size>1:
            k, v = self.pool(k), self.pool(v)
            
        q = rearrange(q, 'b (h c) y x -> b h (y x) c', h=self.heads, y=h, x=w)
        k = rearrange(k, 'b (h c) y x -> b h (y x) c', h=self.heads, y=h//self.pool_size, x=w//self.pool_size)
        v = rearrange(v, 'b (h c) y x -> b h (y x) c', h=self.heads, y=h//self.pool_size, x=w//self.pool_size)

        scaled_dot_prod = torch.einsum('b h i d , b h j d -> b h i j', q, k)
        
        if self.use_cos_sim:
            q_norm = torch.norm(q, dim=-1, keepdim=True)
            k_norm = rearrange(torch.norm(k, dim=-1, keepdim=True), 'b h d c -> b h c d')
            norm = q_norm * k_norm
            scaled_dot_prod /= norm
            scaled_dot_prod = (self.sigma * scaled_dot_prod.permute(0, 2, 3, 1)).permute(0, 3, 1, 2)
        else:
            scaled_dot_prod /= self.alpha
        
        attention = torch.softmax(scaled_dot_prod, dim=-1)
        
        if self.save_attentions:
            timestep = str(int(time.time() * 1e6))
            out_folder = './out'
            batch_folder = os.path.join(out_folder, timestep)

            os.makedirs(batch_folder, exist_ok=True)

            for idx, attn_map in enumerate(attention.detach().cpu().numpy()):
                np.save(os.path.join(batch_folder, '{}.npy'.format(idx)), attn_map)

        if self.save_attentions:
           self.save_attention_maps(attention)

        out = rearrange(torch.einsum('b h i j , b h j d -> b h i d', attention, v),
                        'b h (y x) c -> b (h c) y x',  h=self.heads, y=h, x=w)
            
        out = self.conv_out(out)
        return self.gamma * out
    
    
    def save_attention_maps(self, attention):
        timestep = str(int(time.time() * 1e6))
        out_folder = './out'
        batch_folder = os.path.join(out_folder, timestep)
        
        os.makedirs(batch_folder, exist_ok=True)

        for idx, attn_map in enumerate(attention.detach().cpu().numpy()):
            img = (attn_map[0, :, :, np.newaxis] - attn_map.min()) / (attn_map.max() - attn_map.min()) * 255
            cv2.imwrite(os.path.join(batch_folder, '{}.png'.format(idx)), 
                        img.astype('uint8'))

    
class DoubleSelfAttention(nn.Module):
    def __init__(self, in_dim, skip=False, head_size=-1, factor=1, use_bn=True, use_cos_sim=True, **params):
        super().__init__()
        self.attn_1 = SelfAttention(in_dim, head_size=head_size, factor=factor, use_bn=use_bn, use_cos_sim=use_cos_sim, **params)
        self.attn_2 = SelfAttention(in_dim, head_size=head_size, factor=factor, use_bn=use_bn, use_cos_sim=use_cos_sim, **params)
        self.skip = skip
        

    def forward(self, x1, x2, mask=None):
        assert (x1.dim() == 4) and (x2.dim() == 4), '4D tensor must be provided'
        if self.skip:
            return x1, x2
        
        out1 = self.attn_1(x1)
        out2 = self.attn_2(x2)
            
        out1 = x1 + out1
        out2 = x2 + out2
        
        return out1, out2