import torch.nn as nn
from model_training.models.attention_blocks import DoubleSelfAttention
   

class DoubleNet(nn.Module):
    def __init__(self, model_l, model_r, attn_params):
        super(DoubleNet, self).__init__()
        self.model_l = model_l
        self.model_r = model_r
        
        self.use_attention = attn_params.pop('use_attention')
        
        if self.use_attention:
            encoder_skip = attn_params.pop('encoder_skip_layers')
            decoder_skip = attn_params.pop('decoder_skip_layers')
            self.encoder_attns = nn.ModuleList([DoubleSelfAttention(64, skip=encoder_skip['layer1'], pool_size=2, **attn_params),
                                                DoubleSelfAttention(128, skip=encoder_skip['layer2'], **attn_params), 
                                                DoubleSelfAttention(256, skip=encoder_skip['layer3'], **attn_params), 
                                                DoubleSelfAttention(512, skip=encoder_skip['layer4'], **attn_params)])
            
            self.decoder_attns = nn.ModuleList([DoubleSelfAttention(512, skip=decoder_skip['layer1'], **attn_params),
                                                DoubleSelfAttention(256, skip=decoder_skip['layer2'], **attn_params), 
                                                DoubleSelfAttention(128, skip=decoder_skip['layer3'], **attn_params), 
                                                DoubleSelfAttention(64, skip=decoder_skip['layer4'], pool_size=2, **attn_params)])
        
        
    def forward(self, input):
        features_l, features_r = self._calc_encoders(input)
        decoder_output_l, decoder_output_r = self._calc_decoders(features_l, features_r)

        masks_l = self.model_l.segmentation_head(decoder_output_l)
        masks_r = self.model_r.segmentation_head(decoder_output_r)

        if self.model_l.classification_head is not None and self.model_r.classification_head is not None:
            labels_l = self.model_l.classification_head(features_l[-1])
            labels_r = self.model_r.classification_head(features_r[-1])
            return masks_l, masks_r, labels_l, labels_r

        return masks_l, masks_r

    
    def _calc_encoders(self, input):
        features_l = [input]
        features_r = [input]
        
        x1 = self.model_l.encoder.conv1(input)
        x1 = self.model_l.encoder.bn1(x1)
        x1 = self.model_l.encoder.relu(x1)
        features_l.append(x1)
        x1 = self.model_l.encoder.maxpool(x1)
        
        x2 = self.model_r.encoder.conv1(input)
        x2 = self.model_r.encoder.bn1(x2)
        x2 = self.model_r.encoder.relu(x2)
        features_r.append(x2)
        x2 = self.model_r.encoder.maxpool(x2)

        x1 = self.model_l.encoder.layer1(x1)
        x2 = self.model_r.encoder.layer1(x2)
        if self.use_attention:
            x1, x2 = self.encoder_attns[0](x1, x2)
        
        features_l.append(x1)
        features_r.append(x2)
        
        x1 = self.model_l.encoder.layer2(x1)
        x2 = self.model_r.encoder.layer2(x2)
        if self.use_attention:
            x1, x2 = self.encoder_attns[1](x1, x2)
        
        features_l.append(x1)
        features_r.append(x2)
        
        x1 = self.model_l.encoder.layer3(x1)
        x2 = self.model_r.encoder.layer3(x2)
        if self.use_attention:
            x1, x2 = self.encoder_attns[2](x1, x2)
        
        features_l.append(x1)
        features_r.append(x2)
        
        x1 = self.model_l.encoder.layer4(x1)
        x2 = self.model_r.encoder.layer4(x2)
        if self.use_attention:
            x1, x2 = self.encoder_attns[3](x1, x2)
        
        features_l.append(x1)
        features_r.append(x2)

        return features_l, features_r
    
    
    def _calc_decoders(self, features_l, features_r):
        features_l = features_l[1:][::-1]
        features_r = features_r[1:][::-1]

        head_l = features_l[0]
        head_r = features_r[0]
        
        skips_l = features_l[1:]
        skips_r = features_r[1:]

        decoder_output_l = self.model_l.decoder.center(head_l)
        decoder_output_r = self.model_r.decoder.center(head_r)
        if self.use_attention:
            decoder_output_l, decoder_output_r = self.decoder_attns[0](decoder_output_l, decoder_output_r)
        
        for i, (decoder_block_l, decoder_block_r) in enumerate(zip(self.model_l.decoder.blocks, self.model_r.decoder.blocks)):
            skip_l = skips_l[i] if i < len(skips_l) else None
            skip_r = skips_r[i] if i < len(skips_r) else None
            decoder_output_l = decoder_block_l(decoder_output_l, skip_l)
            decoder_output_r = decoder_block_r(decoder_output_r, skip_r)
            if self.use_attention and i<len(self.decoder_attns)-1:
                decoder_output_l, decoder_output_r = self.decoder_attns[i+1](decoder_output_l, decoder_output_r)

        return decoder_output_l, decoder_output_r
