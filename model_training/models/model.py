import segmentation_models_pytorch as smp
from model_training.models.double_net import DoubleNet


def make_model(config, use_cps=True):
    if use_cps:
        model_l = getattr(smp, config['model']['model_name'])(encoder_name=config['model'].get('backbone', 'resnet34'),
                                                              encoder_weights=config['model'].get('weights', 'imagenet'),
                                                              decoder_attention_type=config['model'].get('attention', None),
                                                              classes=config['model'].get('num_classes', 1),
                                                              in_channels=config['model'].get('in_channels', 3),
                                                              activation=config['model'].get('activation', None))
        model_r = getattr(smp, config['model']['model_name'])(encoder_name=config['model'].get('backbone', 'resnet34'),
                                                              encoder_weights=config['model'].get('weights', 'imagenet'),
                                                              decoder_attention_type=config['model'].get('attention', None),
                                                              classes=config['model'].get('num_classes', 1),
                                                              in_channels=config['model'].get('in_channels', 3),
                                                              activation=config['model'].get('activation', None))
        
        model = DoubleNet(model_l, model_r, config['model']['attn_params'])
    else:
        model = getattr(smp, config['model']['model_name'])(encoder_name=config['model'].get('backbone', 'resnet34'),
                                                            encoder_weights=config['model'].get('weights', 'imagenet'),
                                                            decoder_attention_type=config['model'].get('attention', None),
                                                            classes=config['model'].get('num_classes', 1),
                                                            in_channels=config['model'].get('in_channels', 3),
                                                            activation=config['model'].get('activation', None))
        
    return model