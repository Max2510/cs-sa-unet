import math
from typing import TypeVar, Optional, List, Iterator

from torch.utils.data import Dataset
from torch.utils.data.distributed import DistributedSampler


T_co = TypeVar('T_co', covariant=True)


class ValDistributedSampler(DistributedSampler[T_co]):
    def __init__(self, dataset: Dataset, num_replicas: Optional[int] = None,
                 rank: Optional[int] = None, seed: int = 0) -> None:
        super().__init__(dataset, num_replicas=num_replicas, rank=rank,
                         shuffle=False, seed=seed, drop_last=False)
        
        self.num_samples_per_proc = dataset.num_samples_per_proc
        self.num_samples = self.num_samples_per_proc[self.rank]
        self.total_size = sum(self.num_samples_per_proc)
        assert sum(self.num_samples_per_proc)==len(dataset)

    def __iter__(self) -> Iterator[T_co]:
        indices = list(range(len(self.dataset)))
        
        # subsample
        start_idx = sum(self.num_samples_per_proc[:self.rank]) if self.rank>0 else 0
        indices = indices[start_idx:start_idx + self.num_samples]

        return iter(indices)
