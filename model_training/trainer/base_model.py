import torch
import torch.optim as optim
from joblib import cpu_count
import pytorch_lightning as pl
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from torch.utils.data.distributed import DistributedSampler

from pytorch_forecasting.optim import Ranger
from model_training.metrics.metrics import MetricCounter
from model_training.utils.lightning import load_from_lightning
from model_training.samplers.distributed_sampler import ValDistributedSampler

class BaseModel(pl.LightningModule):
    def __init__(self, config, model, criterion, train, val, test=None, metric_counter=None, sampler=None):
        super().__init__()
        self.config = config
        
        self.model = model
        self._load_model()
        
        self.train_dataset = train
        self.val_dataset = val
        self.test_dataset = test
        self.sampler = sampler
        
        self.metric_counter = metric_counter if not metric_counter is None else MetricCounter()
        self.criterion = criterion

        self.use_ddp = config['trainer']['devices']
        self.save_val_preds = False
        
        self.save_hyperparameters({'config': config})

    
    def forward(self, x):
        return self.model(x)

    
    def training_step(self, batch, batch_nb):
        images, targets = batch['img'], batch['mask']
            
        outputs = self.forward(images)
        loss = self.criterion(outputs, targets)
        
        self.log('train_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)
        self.log('lr', self.learning_rate, on_step=False, on_epoch=True, prog_bar=False, logger=True, sync_dist=True)
        return loss
    

    def validation_step(self, batch, batch_nb):
        images, targets, names = batch['img'], batch['mask'], batch['name']
        outputs = self.forward(images)
        loss = self.criterion(outputs, targets)
        
        self.metric_counter.calc_metrics(outputs, targets, batch_nb, self.global_rank)
        self.metric_counter.add_metrics({'val_loss': loss.item()})

        
    def validation_epoch_end(self, outputs):
        tensorboard_logs = self.metric_counter.get_metrics()
        self.metric_counter.clear()
        self.log_dict(tensorboard_logs, sync_dist=True)
        print('\nLogs: {}'.format(tensorboard_logs))
        
    
    def on_test_start(self):
        self.metric_counter.slices_per_scans = self.test_dataset.slices_per_scans
    
    
    def test_step(self, batch, batch_nb):
        images = batch['img']
        targets = batch['mask'] if 'mask' in batch.keys() else None
        
        outputs = self.forward(images)
        
        self.metric_counter.calc_metrics(outputs, targets, batch_nb, self.global_rank, save_predictions=True)
        
    
    def test_epoch_end(self, outputs):
        tensorboard_logs = self.metric_counter.get_metrics()
        self.metric_counter.clear()
        self.log_dict(tensorboard_logs, sync_dist=True)

        
    def configure_optimizers(self):
        self.optimizer = self._get_optim(filter(lambda p: p.requires_grad, self.model.parameters()))
            
        scheduler = self._get_scheduler(self.optimizer)
        if scheduler is None:
            return self.optimizer
        else:
            return {"optimizer": self.optimizer, 
                    "lr_scheduler": {'scheduler': scheduler, 
                                     'monitor': self.config['scheduler'].get('monitor', 'val_loss')
                                    }
                   }

        
    def train_dataloader(self):
        return self._get_dataloader(self.train_dataset, train=True)

    
    def val_dataloader(self):
        return self._get_dataloader(self.val_dataset, train=False)
    
    
    def test_dataloader(self):
        return self._get_dataloader(self.test_dataset, train=False)
    
    
    @property
    def learning_rate(self):
        return self.optimizer.param_groups[0]['lr']
    
    
    def _load_model(self):
        print("Loading model weights...")
        
        weights_path = self.config['model'].get('weights_path', None)
        if not weights_path is None:
            if 'h5' in weights_path:
                checkpoint = torch.load(weights_path)
                self.model.load_state_dict(checkpoint)
            else:
                self.model = load_from_lightning(self.model, 
                                                 weights_path,
                                                 transform=self.config['model'].get('transform', False),
                                                 pretraining=self.config['model'].get('pretraining', 'middle'))
                
                
    def _get_num_workers(self):
        if 'num_workers' not in self.config['dataloader']:
            return cpu_count()
        if isinstance(self.config['dataloader']['num_workers'], float):
            return int(cpu_count() * self.config['dataloader']['num_workers'])
        return self.config['dataloader']['num_workers']

    
    def _get_dataloader(self, dataset, train=True):
        train_sampler = RandomSampler if not self.use_ddp else DistributedSampler
        val_sampler = SequentialSampler if not self.use_ddp else ValDistributedSampler
        
        labeled_loader = DataLoader(
                    dataset,
                    sampler=train_sampler(dataset) if train else val_sampler(dataset),
                    batch_size=self.config['dataloader']['batch_size'],
                    num_workers=self._get_num_workers(),
                    drop_last=True if train else False) 
        
        return labeled_loader

    
    def _get_optim(self, params, lr=None, optimizer_config=None):
        if optimizer_config is None:
            optimizer_config = self.config['optimizer']
        
        if lr is None:
            lr = optimizer_config['lr']
         
        if optimizer_config['name'] == 'adam':
            optimizer = optim.Adam(params, lr=lr, 
                                   weight_decay=optimizer_config.get('weight_decay', 0.))
        elif optimizer_config['name'] == 'adamw':
            optimizer = optim.AdamW(params, lr=lr,
                                    weight_decay=optimizer_config.get('weight_decay', 1e-2))
        elif optimizer_config["name"] == "rmsprop":
            optimizer = optim.RMSprop(params, lr=lr,
                                      weight_decay=optimizer_config.get('weight_decay', 0.),
                                      momentum=optimizer_config.get('momentum', 0.),
                                      centered=optimizer_config.get('centered', False))
        elif optimizer_config['name'] == 'sgd':
            optimizer = optim.SGD(params,
                                  lr=lr,
                                  momentum=optimizer_config.get('momentum', 0),
                                  weight_decay=optimizer_config.get('weight_decay', 0))
        elif optimizer_config['name'] == 'ranger':
            optimizer = Ranger(params, lr=lr,
                               alpha=optimizer_config.get('alpha', 0.5),
                               k=optimizer_config.get('k', 6),
                               weight_decay=optimizer_config.get('weight_decay', 0.))
        else:
            raise ValueError("Optimizer [%s] not recognized." % optimizer_config['name'])

        return optimizer

    
    def _get_scheduler(self, optimizer):
        scheduler_config = self.config['scheduler']
        if scheduler_config['name'] == 'plateau':
            scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                             mode=scheduler_config.get('mode', 'min'),
                                                             patience=scheduler_config.get('patience', 10),
                                                             factor=scheduler_config.get('factor', 0.1),
                                                             min_lr=scheduler_config.get('min_lr', 0.),
                                                             verbose=scheduler_config.get('verbose', True))
        elif scheduler_config['name'] == 'MultiStepLR':
            scheduler = optim.lr_scheduler.MultiStepLR(optimizer,
                                                       scheduler_config['milestones'],
                                                       gamma=scheduler_config.get('gamma', 0.1))
        elif scheduler_config['name'] == 'cosine':
            scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer,
                                                             scheduler_config['T_max'],
                                                             eta_min=scheduler_config.get('eta_min', 0.),
                                                             verbose=scheduler_config.get('verbose', True))
        elif scheduler_config['name']=='constant':
            scheduler = None
        else:
            raise ValueError("Scheduler [%s] not recognized." % scheduler_config['name'])
        return scheduler
