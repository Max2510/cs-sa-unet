import torch
import torch.nn as nn
from copy import deepcopy
from model_training.metrics.metrics import MetricCounter
from model_training.trainer.base_model import BaseModel

class CPSModel(BaseModel):
    def __init__(self, config, model, criterion, cps_criterion, 
                 train, val, test=None, metric_counter=None, sampler=None):
        super(BaseModel, self).__init__()
        self.config = config
        
        self.model = model
        self._load_model()
        
        if self.config.get('ema', 0.) > 0.:
            self.avg_model = ModelEMA(self.model, self.config['ema'])
        
        self.train_dataset = train
        self.val_dataset = val
        self.test_dataset = test
        self.sampler = sampler
        
        self.metric_counter = metric_counter if not metric_counter is None else MetricCounter()
        self.criterion = criterion
        self.cps_criterion = cps_criterion

        self.use_ddp = config['trainer']['devices']
        self.save_val_preds = False
        
        self.save_hyperparameters({'config': config})
    
    def forward(self, x):
        if self.config.get('ema', 0.) > 0.:
            preds = self.avg_model(x)
        else:
            preds = self.model(x)
            
        return preds[0] + preds[1]

    
    def training_step(self, batch, batch_nb):
        images, targets = batch['img'], batch['mask']

        outputs_l, outputs_r = self.model(images)
        
        if self.config.get('ema', 0.) > 0.:
            with torch.no_grad():
                pseudo_labels_l, pseudo_labels_r = self.avg_model(images)
        else:
            pseudo_labels_l = outputs_l.detach()
            pseudo_labels_r = outputs_r.detach()
                
               
        if self.config['hard_pseudo_labels']:
            _, pseudo_labels_l = torch.max(pseudo_labels_l, dim=1)
            _, pseudo_labels_r = torch.max(pseudo_labels_r, dim=1)
            
        cps_loss = (self.cps_criterion(outputs_l, pseudo_labels_r) + 
                    self.cps_criterion(outputs_r, pseudo_labels_l)) / 2
            
        loss_sv = (self.criterion(outputs_l, targets) + 
                   self.criterion(outputs_r, targets)) / 2
        
        if self.current_epoch>=self.config.get('cps_wait_epochs', 0):
            loss = loss_sv + self.config['cps_weight'] * cps_loss
        else:
            loss = loss_sv
        
        self.log('train_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)
        self.log('loss_sv', loss_sv, on_step=True, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)
        self.log('loss_cps', cps_loss, on_step=True, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)
        self.log('lr', self.learning_rate, on_step=False, on_epoch=True, prog_bar=False, logger=True, sync_dist=True)
        
        if self.config.get('ema', 0.) > 0.:
            self.avg_model.update_parameters(self.model)
    
        return loss


class ModelEMA(nn.Module):
    def __init__(self, model, decay=0.9999, device=None):
        super().__init__()
        self.module = deepcopy(model)
        self.module.eval()
        self.decay = decay
        self.device = device
        if self.device is not None:
            self.module.to(device=device)

    def forward(self, input):
        return self.module(input)

    def _update(self, model, update_fn):
        with torch.no_grad():
            for ema_v, model_v in zip(self.module.parameters(), model.parameters()):
                if self.device is not None:
                    model_v = model_v.to(device=self.device)
                ema_v.copy_(update_fn(ema_v, model_v))
            for ema_v, model_v in zip(self.module.buffers(), model.buffers()):
                if self.device is not None:
                    model_v = model_v.to(device=self.device)
                ema_v.copy_(model_v)

    def update_parameters(self, model):
        self._update(model, update_fn=lambda e, m: self.decay * e + (1. - self.decay) * m)

    def state_dict(self, *args, **kwargs):
        return self.module.state_dict(*args, **kwargs)

    def load_state_dict(self, state_dict):
        self.module.load_state_dict(state_dict)
