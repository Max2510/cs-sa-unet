import os

from pytorch_lightning import Trainer
from pytorch_lightning.loggers.test_tube import TestTubeLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping


def _init_logger(config, fold_idx=0):
    if config.get('debug', False):
        return
    else:
        tt_logger = TestTubeLogger(
            save_dir=os.path.join(config['experiment']['folder'], config['experiment']['name']),
            name='logs',
            version='{}/fold_{}'.format(config['experiment']['version'], fold_idx) if 'version' in config['experiment'].keys() else None,
            debug=False,
            create_git_tag=False
        )
        return tt_logger


def _checkpoint_callback(config, fold_idx):
    checkpoint_callback = ModelCheckpoint(
        dirpath=os.path.join(config['experiment']['folder'], config['experiment']['name'], 'checkpoints', 
                              '_'.join(['version', str(config['experiment']['version'])]), 'fold_{}'.format(fold_idx)),
        save_top_k=config['checkpoint_callback'].get('save_top_k', 1),
        monitor=config['checkpoint_callback'].get('monitor', 'val_loss'),
        mode=config['checkpoint_callback'].get('mode', 'min'),
        verbose=config['checkpoint_callback'].get('verbose', True)
    )
    return checkpoint_callback


def _early_stop_callback(config):
    early_stop_callback = EarlyStopping(
        monitor=config['early_stop_callback'].get('monitor', 'val_loss'),
        min_delta=config['early_stop_callback'].get('min_delta', 0.00),
        patience=config['early_stop_callback'].get('patience', 1000),
        mode=config['early_stop_callback'].get('mode', 'min'),
        verbose=config['early_stop_callback'].get('verbose', False)
    )
    return early_stop_callback


def create_trainer(config, fold_idx=0):
    logger = _init_logger(config, fold_idx)
    callbacks = [_checkpoint_callback(config, fold_idx)] if not config['debug'] else list()
        
    if 'early_stop_callback' in config.keys():
        callbacks.append(_early_stop_callback(config))

    trainer = Trainer(
        **config['trainer'],
        logger=logger,
        enable_checkpointing=False if config.get('debug', False) else True,
        callbacks=callbacks
    )
    return trainer
