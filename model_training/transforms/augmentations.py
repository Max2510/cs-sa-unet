import albumentations as A
import cv2
import copy
import numpy as np


class Augmentation:
    def __init__(self, config, mode='train'):
        assert mode in ['train', 'unlabeled', 'val', 'test', 'inference']
        self.mode = mode
        
        blur_trfms = config.get('blur', {'blur': {'p': 0.}, 'gauss_blur': {'p': 0.}, 'median_blur': {'p': 0.}, 'p': 0.})
        color_trfms = config.get('color', {'color_jitter': {'p': 0.}, 'gamma': {'p': 0.}, 'p': 0.})
        holes_trfms = config.get('holes', {'cutout': {'p': 0.}, 'dropout': {'p': 0.}, 'p': 0.})
        
        if mode=='train' or mode=='unlabeled':
            self.transform = A.Compose([
                                 A.CenterCrop(**config.get('center_crop', {'height': 512, 'width': 512, 'p': 0.})),
                                 A.HorizontalFlip(**config.get('hflip', {'p': 0.})),
                                 A.ShiftScaleRotate(**config.get('shift_scale_rot', {'p': 0.})),
                                 A.OneOf([
                                     A.Blur(**blur_trfms.get('blur', {'p': 1.})),
                                     A.GaussianBlur(**blur_trfms.get('gauss_blur', {'p': 1.})),
                                     A.MedianBlur(**blur_trfms.get('median_blur', {'p': 1.})),
                                 ], p=blur_trfms['p']),
                                 A.OneOf([
                                     A.RandomBrightnessContrast(**blur_trfms.get('color_jitter', {'p': 1.})),
                                     A.RandomGamma(**blur_trfms.get('gamma', {'p': 1.})),
                                 ], p=color_trfms['p']),
                                 A.Superpixels(**config.get('superpixels', {'p': 0.})),
                                 A.Sharpen(**config.get('sharpen', {'p': 0.})),
                                 A.OneOf([
                                     A.Cutout(**holes_trfms.get('cutout', {'p': 1.})),
                                     A.GridDropout(**holes_trfms.get('dropout', {'p': 1.})),
                                 ], p=holes_trfms['p']),
                                 A.RandomCrop(**config.get('random_crop', {'height': 512, 'width': 512, 'p': 0.})),
                                 A.Normalize(**config.get('normalize', {'p': 0.}))
                            ]
            )
        
            
        elif mode == 'val' or mode == 'test' or mode == 'inference':
            self.transform = A.Compose(
                [A.CenterCrop(**config.get('center_crop', {'height': 512, 'width': 512, 'p': 0.})),
                 A.Normalize(**config.get('normalize', {'p': 0.}))
                ]
            )

    def __call__(self, img, mask=None):
        if mask is None:
            augmented = self.transform(image=img)
            return augmented['image']
        else:
            augmented = self.transform(image=img, mask=mask)
            return augmented['image'], augmented['mask']