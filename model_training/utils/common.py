import yaml
import os
from glob import glob
import numpy as np

import torch
from skimage.morphology import label as cc_label

import SimpleITK as sitk

def load_itk_image(filename):
    itkimage = sitk.ReadImage(filename)
    numpyImage = sitk.GetArrayFromImage(itkimage)

    numpyOrigin = np.array(list(reversed(itkimage.GetOrigin())))
    numpySpacing = np.array(list(reversed(itkimage.GetSpacing())))

    return numpyImage, numpyOrigin, numpySpacing


def save_nii_tensor(nii_data_path, log_dir, fold_idx, names, scans_per_gpu):
    output_path = os.path.join(log_dir, 'fold_{}'.format(fold_idx), 'test_preds')
    os.makedirs(output_path, exist_ok=True)
    
    def save_prediction(pred, logits, scan_idx, rank):
        proc_start_scan_idx = sum(scans_per_gpu[:rank]) if rank>0 else 0
        sample_name = names[proc_start_scan_idx+scan_idx]
        sitk_img = os.path.join(nii_data_path, '{}.nii.gz'.format(sample_name, sample_name))
        _, origin, spacing = load_itk_image(sitk_img)
        itkimage = sitk.GetImageFromArray(pred, isVector=False)
        itkimage.SetSpacing(np.array(list(reversed(spacing))))
        itkimage.SetOrigin(np.array(list(reversed(origin))))
        sitk.WriteImage(itkimage, os.path.join(output_path, '{}.nii'.format(sample_name)))
        
        np.save(os.path.join(output_path, '{}.npy'.format(sample_name)), logits)
        
    return save_prediction
    
    
def load_yaml(x: str):
    with open(x) as fd:
        return yaml.load(fd, yaml.FullLoader)


def get_names_list(data_path):
    train_imgs = sorted(glob(os.path.join(data_path, 'Patient*')))
    train_imgs = [os.path.basename(img) for img in train_imgs]

    return train_imgs


def cc_filter(pred, num_classes, cc_thresh=0.05):
    for cls in range(num_classes):
        if isinstance(pred, torch.Tensor):
            pred_cls = (pred==cls+1).type_as(pred)
            labels = torch.Tensor(cc_label(pred_cls.cpu().numpy(), connectivity=2)).type_as(pred)
            un_labels, num_voxels = torch.unique(labels, return_counts=True)
        else:
            pred_cls = pred==cls+1
            labels = cc_label(pred_cls, connectivity=2)
            un_labels, num_voxels = np.unique(labels, return_counts=True)
            
        un_labels, num_voxels = un_labels[1:], num_voxels[1:]
        unvalid_labels = un_labels[num_voxels / num_voxels.sum() < cc_thresh]
        if len(unvalid_labels) < 100:
            for unvalid_label in unvalid_labels:
                pred[labels==unvalid_label] = 0
                
        return pred