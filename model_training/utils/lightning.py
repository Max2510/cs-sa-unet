import torch
from collections import OrderedDict


def load_from_lightning(model: torch.nn.Module, checkpoint_path: str, sd_name=None):
    checkpoint = torch.load(checkpoint_path)
    model_state_dict = model.state_dict()

    if 'state_dict' in checkpoint.keys():
        ckpt_state_dict = OrderedDict()
        if not sd_name is None:
            checkpoint['state_dict'] = {k: v for k, v in checkpoint['state_dict'].items() if k.split('.')[0]==sd_name}
        
        for k, v in checkpoint['state_dict'].items():
            new_key = '.'.join(k.split('.')[1:])
            ckpt_state_dict[new_key] = v
    else:
        ckpt_state_dict = {k: v for k, v in checkpoint.items()}
    
    model_state_dict.update(ckpt_state_dict)
    model.load_state_dict(model_state_dict)
    return model


def get_state_dict(checkpoint_path: str):
    checkpoint = torch.load(checkpoint_path)
    return {k.lstrip('model').lstrip('.'): v for k, v in checkpoint['state_dict'].items()}
