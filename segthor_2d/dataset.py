import os
import png
from glob import glob
import numpy as np

from torch.utils.data import Dataset

from model_training.transforms.augmentations import Augmentation

class SegTHORDataset(Dataset):
    def __init__(self, config, pat_names, data_path, num_gpus=1, mode='train'):
        self.data_path = data_path
        self.pat_names = pat_names
        self.num_classes = config['model']['num_classes']
        self.num_gpus = num_gpus
        self.mode = mode
        
        self.img_names = [x for name in pat_names 
                          for x in sorted(glob(os.path.join(self.data_path, '{}/{}*'.format(name, name))))]
        
        if self.mode!='inference':
            self.mask_names = [x for name in pat_names 
                               for x in sorted(glob(os.path.join(self.data_path, '{}/GT*'.format(name))))]
        
        self.transformer = Augmentation(config['trfm'], mode=mode)
        
        if mode!='train':
            self.calc_slices_per_scans_and_procs()
    
    
    def calc_slices_per_scans_and_procs(self):
        _, slices_per_scans = np.unique([name.split('/')[-2] for name in self.img_names], return_counts=True)
        self.scans_per_gpu = np.round(np.arange(0., len(slices_per_scans)*(1+1/self.num_gpus), 
                                                len(slices_per_scans)/self.num_gpus)).astype(int)
        
        self.slices_per_scans = [slices_per_scans[self.scans_per_gpu[i]:self.scans_per_gpu[i+1]] 
                                 for i in range(len(self.scans_per_gpu)-1)]
        self.num_samples_per_proc = [np.sum(x) for x in self.slices_per_scans]
        
        
    def __len__(self):  
        return len(self.img_names)
        

    def __getitem__(self, idx):
        with open(self.img_names[idx], 'rb') as f:
            r = png.Reader(file=f)
            data = r.read()
            img = np.vstack(list(map(np.int16, data[2])))
              
        img = (np.expand_dims(img, 2).repeat(3, 2) / 2000.).astype(np.float32)
        
        if self.mode=='inference':
            img = self.transformer(img)
            return {'img': np.transpose(img, (2, 0, 1)), 'name': os.path.basename(self.img_names[idx])}
            
        else:
            with open(self.mask_names[idx], 'rb') as f:
                r = png.Reader(file=f)
                data = r.read()
                mask = np.vstack(list(map(np.uint8, data[2])))


            img, mask = self.transformer(img, mask)
    
            return {'img': np.transpose(img, (2, 0, 1)), 'mask': mask.astype(np.int64), 'name': os.path.basename(self.img_names[idx])}
    