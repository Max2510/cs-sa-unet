import os
from glob import glob
from scipy.special import softmax
import numpy as np
import SimpleITK as sitk

from model_training.utils.common import load_itk_image


if __name__ == '__main__':
    exp_folder = 'experiments'
    exp_name = 'single_model'
    version = 1
    
    nii_data_path = '/data1/maksym/datasets/SegTHOR/test'
    out_folder = test_folders = os.path.join(exp_folder, exp_name, 'logs', f'version_{version}', 'ensemble_preds')
    os.makedirs(out_folder, exist_ok=True)
    
    test_folders = sorted(glob(os.path.join(exp_folder, exp_name, 'logs',
                                            f'version_{version}', f'fold_*', 'test_preds')))
    
    test_preds = [sorted(glob(os.path.join(test_folder, '*.npy'))) for test_folder in test_folders]
    for sample_paths in zip(*test_preds):
        sample = np.array([np.load(path) for path in sample_paths])
        out = softmax(np.array(sample), axis=2).sum(0).argmax(1)
        
        sample_name = os.path.splitext(os.path.basename(sample_paths[0]))[0]
        sitk_img = os.path.join(nii_data_path, '{}.nii.gz'.format(sample_name, sample_name))
        _, origin, spacing = load_itk_image(sitk_img)
        itkimage = sitk.GetImageFromArray(out, isVector=False)
        itkimage.SetSpacing(np.array(list(reversed(spacing))))
        itkimage.SetOrigin(np.array(list(reversed(origin))))
        sitk.WriteImage(itkimage, os.path.join(out_folder, '{}.nii'.format(sample_name)))
		
    
    

