import png
import sys

import numpy as np

import os
import glob
import argparse

from model_training.utils.common import load_itk_image

def preprocess(img_path, output_path, test=False):
    image, _, _ = load_itk_image(img_path)
    image = np.clip(image, -1000, 1000).astype(np.uint16) + 1000

    sample_name = os.path.basename(img_path).split('.')[0]
    os.makedirs(os.path.join(output_path, sample_name), exist_ok=True)
    z_size = image.shape[0]
    
    for slice in range(z_size):
        npy_img_path = os.path.join(output_path, '{}/{}_z{:03d}.png'.format(sample_name, sample_name, slice))
        with open(npy_img_path, 'wb') as f:
            writer = png.Writer(width=image.shape[2], height=image.shape[1], bitdepth=16)
            writer.write(f, image[slice])
    
        if not test:
            mask_path = os.path.join(os.path.dirname(img_path), 'GT.nii.gz')
            mask, _, _ = load_itk_image(mask_path)
            mask = mask.astype('uint8')
        
            npy_mask_path = os.path.join(output_path, '{}/GT_z{:03d}.png'.format(sample_name, slice))
            with open(npy_mask_path, 'wb') as f:
                writer = png.Writer(width=mask.shape[2], height=mask.shape[1], bitdepth=8)
                writer.write(f, mask[slice])


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test', action='store_true')
    args = parser.parse_args()
    
    if args.test:
        nii_path = '/data1/maksym/phd/datasets/SegTHOR/test'
        output_path = '/data1/maksym/phd/datasets/SegTHOR/test/pp_2d'
        imgs = sorted(glob.glob(os.path.join(nii_path, 'Patient*.nii.gz')))
    else:
        nii_path = '/data1/maksym/phd/datasets/SegTHOR/train'
        output_path = '/data1/maksym/phd/datasets/SegTHOR/train/pp_2d'
        imgs = sorted(glob.glob(os.path.join(nii_path, 'Patient*', 'Patient*.nii.gz')))

    for sample_idx, img_path in enumerate(imgs):
        preprocess(img_path, output_path, test=args.test)
        print('Converted files: {}/{}'.format(sample_idx + 1, len(imgs)))
        sys.stdout.flush()