import os

from sklearn.model_selection import KFold

import torch
from pytorch_lightning.utilities.seed import seed_everything

from model_training.utils.common import load_yaml, get_names_list, save_nii_tensor
from model_training.models.model import make_model
from model_training.losses.loss import get_loss
from model_training.metrics.metrics import MetricCalculator2D
from model_training.metrics.dice_score import dice_metric
from model_training.trainer.base_model import BaseModel
from model_training.trainer.cps_model import CPSModel
from model_training.trainer.trainer_utils import create_trainer

from segthor_2d.dataset import SegTHORDataset


seed_everything(42)

if __name__ == '__main__':
    config_path = 'segthor_2d/config/segthor_config.yaml'
    config = load_yaml(config_path)
    
    num_gpus = config['trainer']['devices']
    use_cps = config['use_cps']
    
    names = get_names_list(config['data_path'])
    kf = KFold(n_splits=config['cv']['n_splits'], shuffle=config['cv']['shuffle'], random_state=config['cv']['random_state'])
    for f_idx, (train_indices, val_indices) in enumerate(list(kf.split(names))[config['cv']['start_fold']:config['cv']['start_fold'] + config['cv']['n_split_exec']]):
        fold_idx = f_idx + config['cv']['start_fold']
        train_names = [names[idx] for idx in train_indices]
        val_names = [names[idx] for idx in val_indices]
        test_names = get_names_list(config['test_data_path'])

        train = SegTHORDataset(config, train_names, config['data_path'], num_gpus=num_gpus, mode='train')
        val = SegTHORDataset(config, val_names, config['data_path'], num_gpus=num_gpus, mode='val')
        test = SegTHORDataset(config, test_names, config['test_data_path'], num_gpus=num_gpus, mode='inference')

        model = make_model(config, use_cps=use_cps)

        criterion = get_loss(config['loss'])

        log_folder = os.path.join(config['experiment']['folder'], config['experiment']['name'], 'logs', 
                            '_'.join(['version', str(config['experiment']['version'])]))
        save_predictions = save_nii_tensor(config['nii_data_path'], log_folder, fold_idx,
                                           test_names, test.scans_per_gpu)
        
        metric_counter = MetricCalculator2D(dice_metric, val.slices_per_scans, 
                                            save_predictions_fn=save_predictions,
                                            ori_img_size=config.get('ori_img_size', None))

        
        if not use_cps:
            pl_model = BaseModel(
                config=config,
                model=model,
                criterion=criterion,
                train=train,
                val=val,
                test=test,
                metric_counter=metric_counter
            )
        else:
            pl_model = CPSModel(
                config=config,
                model=model,
                criterion=criterion,
                cps_criterion=get_loss(config['cps_loss']),
                train=train,
                val=val,
                test=test,
                metric_counter=metric_counter
            )


        trainer = create_trainer(config, fold_idx)
        trainer.fit(pl_model)
        
        trainer.validate(ckpt_path='best')    
        trainer.test()

        torch.cuda.empty_cache()

